-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 15-Dez-2016 às 08:36
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ulhtbook`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `amigo`
--

CREATE TABLE `amigo` (
  `id` int(11) NOT NULL,
  `id_amigo` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `amizade` tinyint(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `amigo`
--

INSERT INTO `amigo` (`id`, `id_amigo`, `id_user`, `amizade`) VALUES
(1, 14, 20, 1),
(6, 32, 14, 1),
(3, 1, 14, 1),
(4, 32, 14, 1),
(5, 14, 2, 0),
(7, 2, 14, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `nome` varchar(36) NOT NULL,
  `data_nascimento` varchar(36) NOT NULL,
  `foto_user` varchar(1000) NOT NULL,
  `idade` varchar(36) NOT NULL,
  `password` varchar(36) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id_user`, `nome`, `data_nascimento`, `foto_user`, `idade`, `password`) VALUES
(2, 'ze manel', '08/12/2000', 'http://sorisomail.com/img/126325295337.jpg', '16', 'a123'),
(20, 'zezinho', '22/03/12', 'http://www.gadoo.com.br/wp-content/uploads/2015/08/2297.jpg', '12', 'a'),
(12, 'admin', '12/8//2009', 'http://www.gadoo.com.br/wp-content/uploads/2015/08/2297.jpg', '16', 'a'),
(1, 'kika', '9/8/2000', 'http://4.bp.blogspot.com/-TLkc6QeRjMY/Uhu0bmdVN9I/AAAAAAAAAS0/v-3iuPMC4_k/s1600/animais-engracados-7.jpg', '16', 'k123'),
(14, 'kiko', '9/8/2000', 'http://4.bp.blogspot.com/-TLkc6QeRjMY/Uhu0bmdVN9I/AAAAAAAAAS0/v-3iuPMC4_k/s1600/animais-engracados-7.jpg', '16', 'k123'),
(32, 'flor', '5/5/1995', 'http://videosengracadosyoutube.com/wp-content/uploads/2016/09/1475013538_hqdefault.jpg', '21', 'f123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amigo`
--
ALTER TABLE `amigo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `amigo`
--
ALTER TABLE `amigo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
